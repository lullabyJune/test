const Koa = require('koa')
const app = new Koa()

app.use((ctx) => {
    ctx.body = 'this is from CI docker!'
})

app.listen(8081, () => {
    console.info('now koa in gitlab is running on port 8081')
})